// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <iterator> // istream_iterator
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair
#include <vector>   // since array was overflowing my stack

#include "Collatz.hpp"

using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read(istream_iterator<int> &begin_iterator) {
    int i = *begin_iterator;
    ++begin_iterator;
    int j = *begin_iterator;
    ++begin_iterator;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

// using memoization to store the known cycle lengths
const int DP_LENGTH = 1000000;
vector<int> dp(DP_LENGTH + 1, 0);

int getElementFromDp(int i) {
    return dp[i];
}

tuple<int, int, int> collatz_eval(const pair<int, int> &p) {
    if (dp[1] == 0) {
        // dp array hasn't been filled. Happens during unit testing
        fillDpArray();
    }
    int i;
    int j;
    tie(i, j) = p;
    // check pre-conditions
    assert(i > 0);
    assert(j > 0);
    assert(i < 1000000);
    assert(j < 1000000);

    // make sure the 2 numbers are in order
    int start = i < j ? i : j;
    int end = i < j ? j : i;

    // implement optimization from quiz
    if (start < end / 2 + 1) {
        start = end / 2 + 1;
    }

    int max = 0;
    for (int i = start; i <= end; ++i) {
        int cycleLength = dp[i];
        if (cycleLength > max) {
            max = cycleLength;
        }
    }
    return make_tuple(i, j, max);
}

// -------------
// collatz_print
// -------------

void collatz_print(ostream &sout, const tuple<int, int, int> &t) {
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void fillDpArray() {
    for (long i = 1; i <= 1000000; i++) {
        int cycleLength = 1;
        long iCopy = i;
        while (iCopy != 1) {
            if (iCopy < i) {
                cycleLength += dp[iCopy] - 1;
                iCopy = 1;
            } else if (iCopy % 2 == 0) {
                iCopy /= 2;
                cycleLength++;
            } else {
                iCopy = iCopy + (iCopy >> 1) + 1;
                cycleLength += 2;
            }
        }
        dp[i] = cycleLength;
    }
}


void collatz_solve(istream &sin, ostream &sout) {
    istream_iterator<int> begin_iterator(sin);
    istream_iterator<int> end_iterator;
    fillDpArray();
    while (begin_iterator != end_iterator) {
        collatz_print(sout, collatz_eval(collatz_read(begin_iterator)));
    }
}
