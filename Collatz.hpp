// ---------
// Collatz.h
// ---------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <iterator> // istream_iterator
#include <tuple>    // tuple
#include <utility>  // pair

using namespace std;

// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param an istream_iterator
 * @return a pair of ints
 */
pair<int, int> collatz_read (istream_iterator<int>&);

// ------------
// collatz_eval
// ------------

/**
 * @param a pair of ints
 * @return a tuple of three ints
 */
tuple<int, int, int> collatz_eval (const pair<int, int>&);

/**
 * enable other files to access the dp variable,
 * purely for testing purposes
 * @param the index to access
 * @return the dp value stored in the dp vector
 */
int getElementFromDp(int i);

/**
 * Fill the dp array from 1 to 1000000 with each number's collatz cycle length.
 */
void fillDpArray();

// -------------
// collatz_print
// -------------

/**
 * print three ints
 * @param an ostream
 * @param a tuple of three ints
 */
void collatz_print (ostream&, const tuple<int, int, int>&);

// -------------
// collatz_solve
// -------------

/**
 * @param an istream
 * @param an ostream
 */
void collatz_solve (istream&, ostream&);

#endif // Collatz_h
