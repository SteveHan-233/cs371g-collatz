# CS371g: Generic Programming Collatz Repo

* Name: Chengzhi Han 

* EID: ch47686

* GitLab ID: SteveHan-233

* HackerRank ID: SteveHan

* Git SHA: 1ac946d0fbdecab93cd4528c5fef2903ca56935e

* GitLab Pipelines: https://gitlab.com/SteveHan-233/cs371g-collatz/-/pipelines 

* Estimated completion time: 5

* Actual completion time: 15 

* Comments: spent half of the time stuck on an int overflow error. The problem is I couldn't see the fact that I was trying to access a negative array index from the errors (which just showed segfault, which I interpreted as stack overflow). I'll learn gdb which can hopefully let me pinpoint the issue much quicker. 
